<?php

class Calculator {

    public $num1 = " ";
    public $num2 = " ";
    public $name = "";

    public function prepare($data) {
//        echo "<pre>";
//        print_r($data);
        $this->num1 = $data['number1'];
        $this->num2 = $data['number2'];
        $this->name = $data['name'];
        return $this;
    }

    public function add() {


        $a = $this->num1 + $this->num2;
        echo "output of addition is " . $a . "<br/>";
    }

    public function sub() {


        $a2 = $this->num1 - $this->num2;
        echo "output of substraction is " . $a2 . "<br/>";
    }

    public function mul() {


        $a1 = $this->num1 * $this->num2;
        echo "output of multiplication is " . $a1 . "<br/>";
    }

    public function div() {


        $a3 = $this->num1 / $this->num2;
        echo "output  of division is " . $a3;
    }

}
